﻿@extends('layouts.site')

@section('title', '¡Atrévete a ser único!')

@section('content')
    <section class="menu cid-rDda7Z1mMj" once="menu" id="menu1-7">
        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand">

                    <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-5" href="#top">Mykonos Moda</a></span>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                        <a class="nav-link link text-white display-7" href="index.html#header6-2"><span class="mbrib-home mbr-iconfont mbr-iconfont-btn"></span>Inicio</a>
                    </li><li class="nav-item"><a class="nav-link link text-white display-7" href="index.html#counters4-3"><span class="mbrib-hearth mbr-iconfont mbr-iconfont-btn"></span>¿Por qué nosotros?</a></li><li class="nav-item">
                        <a class="nav-link link text-white display-7" href="#">
                            </a>
                    </li></ul>
                <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-secondary display-4" href="{{ route('register') }}"><span class="mbrib-paper-plane mbr-iconfont mbr-iconfont-btn"></span>

                        Registrarme</a></div>
            </div>
        </nav>
    </section>

    <section class="header6 cid-rDcMjxkcK7 mbr-fullscreen mbr-parallax-background" id="header6-2">
        <div class="mbr-overlay" style="opacity: 0.4; background-color: rgb(35, 35, 35);">
        </div>

        <div class="container">
            <div class="row justify-content-md-center">
                <div class="mbr-white col-md-10">
                    <h1 class="mbr-section-title align-center mbr-bold mbr-fonts-style display-1 m-0">DISEÑOS A TU MEDIDA</h1>
                    <p class="mbr-text align-center pb-3 mbr-fonts-style display-2 first-title">Calzado y chaquetas en cuero</p>
                    <p class="mbr-text align-center pb-5 mbr-fonts-style display-2 second-title"><strong>¡Atrévete a ser único!</strong></p>
                    <div class="mbr-section-btn align-center">
                        <a class="btn btn-md btn-secondary display-7" href="{{ route('register') }}">
                            <span class="mbrib-paper-plane mbr-iconfont mbr-iconfont-btn">
                            </span>
                            REGISTRARME
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="counters4 counters cid-rDcS7uqahc" id="counters4-3">
        <div class="container pt-4 mt-2">
            <h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2">¿Por qué nosotros?<br><br></h2>

            <div class="media-container-row">
                <div class="media-block m-auto" style="width: 60%;">
                    <div class="mbr-figure">
                        <img src="assets/images/whatsapp-image-2019-09-27-at-3.19.44-pm-1268x845.png" alt="" title="">
                    </div>
                </div>
                <div class="cards-block">
                    <div class="cards-container">
                        <div class="card px-3 align-left col-12">
                            <div class="panel-item p-4 d-flex align-items-start">
                                <div class="card-img pr-3">
                                    <h3 class="img-text d-flex align-items-center justify-content-center">
                                        1
                                    </h3>
                                </div>
                                <div class="card-text">
                                    <h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">Calidad</h4>
                                    <p class="mbr-content-text mbr-fonts-style display-7">Diseños de calidad a tu alcance<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="card px-3 align-left col-12">
                            <div class="panel-item p-4 d-flex align-items-start">
                                <div class="card-img pr-3">
                                    <h3 class="img-text d-flex align-items-center justify-content-center">
                                        2
                                    </h3>
                                </div>
                                <div class="card-text">
                                    <h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">Moda</h4>
                                    <p class="mbr-content-text mbr-fonts-style display-7">La moda a tu gusto y medida, elaborada por manos de artesanos<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="card px-3 align-left col-12">
                            <div class="panel-item p-4 d-flex align-items-start">
                                <div class="card-img pr-3">
                                    <h3 class="img-text d-flex align-items-center justify-content-center">
                                        3
                                    </h3>
                                </div>
                                <div class="card-text">
                                    <h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">Experiencia</h4>
                                    <p class="mbr-content-text mbr-fonts-style display-7">Nuestra experiencia, tu satisfacción<br></p>
                                </div>
                            </div>
                        </div>
                        <div class="card px-3 align-left col-12">
                            <div class="panel-item p-4 d-flex align-items-start">
                                <div class="card-img pr-3">
                                    <h3 class="img-text d-flex align-items-center justify-content-center">
                                        4
                                    </h3>
                                </div>
                                <div class="card-texts">
                                    <h4 class="mbr-content-title mbr-bold mbr-fonts-style display-7">Atención personalizada</h4>
                                    <p class="mbr-content-text mbr-fonts-style display-7">Atención personalizada a domicilio</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection