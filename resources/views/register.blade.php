﻿@extends('layouts.site')

@section('title', 'Registro')

@section('content')
  <section class="header15 cid-rEl2F9HzUw cid-rDcMjxkcK7 mbr-fullscreen mbr-parallax-background" id="header15-4">
    <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(35, 35, 35);"></div>
    <div class="container align-right">
        <div class="row">
            <div class="mbr-white col-lg-8 col-md-7 content-container">
              <h1 class="mbr-section-title mbr-bold mbr-fonts-style display-1 m-0 Tright">DISEÑOS A TU MEDIDA</h1>
              <p class="mbr-text pb-3 mbr-fonts-style display-2 first-title Tright">Calzado y chaquetas en cuero</p>
              <p class="mbr-text pb-5 mbr-fonts-style display-2 second-title Tright"><strong>¡ Atrévete a ser único!</strong></p>
            </div>
            <div class="col-lg-4 col-md-5">
                <div class="form-container">
                    <div class="media-container-column">
                        <form @submit="comprobeForm($event)" action="{{ route('post_register') }}" method="POST" class="mbr-form form-with-styler">
                          @csrf
                            <div class="dragArea row">
                                <div class="col-md-12 form-group">
                                  <input
                                    v-model="name"
                                    type="text"
                                    placeholder="Nombre"
                                    name="name"
                                    aria-label="First name"
                                    class="form-control @error('name') is-invalid form-control @enderror"
                                    @if (!empty(old('name')))
                                      value="{{ old('name') }}"
                                    @endif
                                  id="name-header15-4">

                                  <div class="invalid-feedback d-block">
                                    @{{ errorname }}
                                    @error('name')
                                      {{ $message }}
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-md-12 form-group">
                                  <input
                                    v-model="lastname"
                                    type="text"
                                    placeholder="Apellido"
                                    name="lastname"
                                    aria-label="Last name"
                                    class="form-control @error('lastname') is-invalid form-control @enderror"
                                    @if (!empty(old('lastname')))
                                      value="{{ old('lastname') }}"
                                    @endif
                                  id="lastname-header15-4" >

                                  <div class="invalid-feedback d-block">
                                    @{{ errorlastname }}
                                    @error('lastname')
                                      {{ $message }}
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-md-12 form-group">
                                  <select
                                    class="form-control px-3 display-7 custom-select @error('gender') is-invalid form-control form-control px-3 display-7 custom-select @enderror"
                                    v-model="gender"
                                    name="gender"
                                    @if (!empty(old('gender')))
                                      value="{{ old('gender') }}"
                                    @endif
                                  id="gender-header15-4" >
                                    <option disabled selected value="">Genero</option>
                                    <option value="1">Hombre</option>
                                    <option value="2">Mujer</option>
                                    <option value="3">Otros</option>
                                  </select>

                                  <div class="invalid-feedback d-block">
                                    @{{ errorgender }}
                                    @error('gender')
                                      {{ $message }}
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-md-12 form-group">
                                  <input
                                    class="form-control px-3 display-7 @error('phone') is-invalid form-control px-3 display-7 @enderror"
                                    type="tel"
                                    name="phone"
                                    placeholder="Celular"
                                    v-model="phone"
                                    pattern="[0-9]{10}"
                                    @if (!empty(old('phone')))
                                      value="{{ old('phone') }}"
                                    @endif
                                  id="phone-header15-4" >

                                  <div class="invalid-feedback d-block">
                                    @{{ errorphone }}
                                    @error('phone')
                                      {{ $message }}
                                    @enderror
                                  </div>
                                </div>
                                <div class="col-md-12 form-group">
                                  <input
                                    type="email"
                                    name="email"
                                    placeholder="Email"
                                    class="form-control px-3 display-7 @error('email') is-invalid form-control px-3 display-7 @enderror"
                                    v-model="email"
                                    @if (!empty(old('email')))
                                      value="{{ old('email') }}"
                                    @endif
                                  id="email-header15-4">

                                  <div class="invalid-feedback d-block">
                                    @{{ erroremail }}
                                    @error('email')
                                      {{ $message }}
                                    @enderror
                                  </div>
                                </div>

                                <!--<a href="thank-you.html" title="">--><div class="col-md-12 input-group-btn"><button type="submit" class="btn btn-secondary btn-form display-4">Registrarme</button></div><!--</a>-->
                            </div>
                        </form><!---Formbuilder Form--->
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection

@section('scripts')
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="{{ asset('assets/theme/js/script2.js') }}"></script>
@endsection