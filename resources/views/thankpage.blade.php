@extends('layouts.site')

@section('title', 'Gracias')

@section('content')
  <section class="tabs1 cid-rF7FNBdX8f mbr-parallax-background" id="tabs1-7">
    <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(35, 35, 35);">
    </div>
    <div class="container">
        <h2 class="mbr-white align-center pb-5 mbr-fonts-style mbr-bold display-2">
            Hola {{ auth()->user()->name }}.<br>Gracias por registrarte</h2>
        <div class="media-container-row">
            <div class="col-12 col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link mbr-fonts-style show active display-5" role="tab" data-toggle="tab" href="" aria-selected="true" @click="gotoLink(1)">
                        Ir a la tienda
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link mbr-fonts-style show display-5" role="tab" data-toggle="tab" href="" aria-selected="false" @click="gotoLink(2)">
                        Contactanos
                      </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab1" class="tab-pane in active" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="mbr-text py-5 mbr-fonts-style display-5">
                                    Ir a la tienda para obtener productos al instante o personalizarlos mediante la ayuda en el chat.</p>
                            </div>
                        </div>
                    </div>
                    <div id="tab2" class="tab-pane" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="mbr-text py-5 mbr-fonts-style display-5">
                                    Formas de contacto para obtener ayuda de un asesor personalizado.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection

@section('scripts')
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script type="text/javascript">
var app = new Vue({
  el: '#tabs1-7',
  methods:{
    gotoLink: function(link){
      if(link == 1) setInterval(function(){
            location.href = "https://www.facebook.com/mykonoscol/shop";
          },2000,"JavaScript");
      else setInterval(function(){
            location.href = "https://api.whatsapp.com/send?phone=573174793479&text=Hola Mykonos Moda. Tengo la siguiente duda:";
          },2000,"JavaScript");
    }
  }
});
</script>
@endsection