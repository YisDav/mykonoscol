@component('components.main.head')
    @slot('title')
        @yield('title')
    @endslot
    @slot('styles')
        @yield('styles')
    @endslot
@endcomponent

<body class="animsition">
    @yield('content')

    @component('components.main.foot')
        @slot('scripts')
            @yield('scripts')
        @endslot
    @endcomponent
</body>
</html>
<!-- end document-->