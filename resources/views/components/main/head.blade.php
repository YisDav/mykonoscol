<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <base href="{{ env('APP_URL') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/icon.png" type="image/x-icon">
    <meta name="description" content="Calzado y Chaquetas en cuero | Mykonos Moda">
    <meta name="author" content="Jesús García">
    <meta name="keywords" content="calzado, chaquetas, chaquetería, comprar calzado, comprar, productos, cuero">

    <title>{{ $title }} | Mykonos Moda</title>
    <link rel="stylesheet" href="assets/web/assets/mykonos-icons-bold/mykonos-icons-bold.css">
    <link rel="stylesheet" href="assets/web/assets/mykonos-icons2/mykonos2.css">
    <link rel="stylesheet" href="assets/web/assets/mykonos-icons/mykonos-icons.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="preload" as="style" href="assets/mykonos/css/mbr-additional.css"><link rel="stylesheet" href="assets/mykonos/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" href="assets/theme/css/style2.css" type="text/css">
    @isset ($styles)
        {{ $styles }}
    @endisset
</head>