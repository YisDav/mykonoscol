var app = new Vue({
	el: '#header15-4',
	data:
	{
		name: null,
		errorname: null,
		lastname: null,
		errorlastname: null,
		gender: "",
		errorgender: null,
		phone: null,
		errorphone: null,
		email: null,
		erroremail: null,
		hasError: false,
	},
	methods:{
		comprobeForm: function(event){
			if(this.formhasError() == true) event.preventDefault();
			/*event.preventDefault();
			console.log(this.formhasError());*/
		},
		formhasError: function(){
			this.hasError = false; this.errorname = null; this.errorlastname = null; this.errorgender = null; this.errorphone = null; this.erroremail = null;
            var gender = $("#gender-header15-4").val();

            const namesRE = new RegExp('^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$', 'i');
            var emailRE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var phoneRE = /^\d{10}$/;

			if( this.name == null || this.name < 3 || /^\s+$/.test(this.name) || !namesRE.test(this.name) )
			{
				this.hasError = true;
				this.errorname = "Por favor coloca tu nombre";
			}

			else if( this.lastname == null || this.lastname < 3 || /^\s+$/.test(this.lastname) || !namesRE.test(this.lastname) )
			{
				this.hasError = true;
				this.errorlastname = "Por favor coloca tu apellido";
			}

            else if ( gender < 1 || gender > 3 || gender == null)
            {
            	this.hasError = true;
            	this.errorgender = "Por favor coloca tu genero.";
            }

			else if(!phoneRE.test(this.phone))
			{
				this.hasError = true;
            	this.errorphone = "Por favor coloca tu numero de celular (10 digitos)";
			}

			else if(!emailRE.test(this.email))
			{
				this.hasError = true;
            	this.erroremail = "Por favor coloca tu email.";
			}

			return this.hasError;
		},
        oldCampValues: function() {
            var uservalue = $("#name-header15-4").attr("value");
            var user2value = $("#lastname-header15-4").attr("value");
            var phonevalue = $("#phone-header15-4").attr("value");
            var emailvalue = $("#email-header15-4").attr("value");

            this.name = uservalue;
            this.lastname = user2value;
            this.phone = phonevalue;
            this.email = emailvalue;

            var gendervalue = $("#gender-header15-4").attr("value");
            $("#gender-header15-4").val(gendervalue);
        }
	}
});
app.oldCampValues();