<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('/registro', function () {
    return view('register');
})->name('register');
Route::post('/registro', 'Auth\RegisterController@register')->name('post_register');

Route::get('/gracias', function (App\User $user) {
	return view('thankpage', compact('user'));
})->middleware('auth')->name('thank-page');

